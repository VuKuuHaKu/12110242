﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Blog.Models
{
    public class Post
    {
        [Key]
        public int Id { get; set; }
        public String Title { get; set; }
        public String Body { get; set; }
    }
    public class BlogDBContext:DbContext
    {
        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
    }
}